package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<TaskDTO> {

    @Insert("INSERT INTO `tm_task` " +
            "(`id`, `name`, `description`, `dateStart`, `dateFinish`, `userId`, `status`, `projectId`) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{status}, " +
            "#{projectId})")
    void insert(@NotNull TaskDTO task);

    @Update("UPDATE `tm_task` " +
            "SET `userId` = #{userId}, `name` = #{name}, `description` = #{description}, `dateStart` = #{dateStart}, " +
            "`dateFinish` = #{dateFinish}, `status` = #{status}, `projectId` = #{projectId} " +
            "WHERE `id` = #{id}")
    void update(@NotNull TaskDTO task);

    @Delete("DELETE FROM `tm_task` WHERE `userId` = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `tm_task` WHERE `projectId` = #{projectId}")
    void removeAllByProjectId(@NotNull String projectId);

    @Delete("DELETE FROM `tm_task` WHERE `userId` = #{userId} AND `id` = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM `tm_task` WHERE `userId` = #{userId} AND `name` = #{name}")
    void removeByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @NotNull
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId}")
    List<TaskDTO> findAll(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId} AND `projectId` = #{projectId}")
    List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId} AND `id` = #{id} LIMIT 1")
    TaskDTO findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    TaskDTO findByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM `tm_task` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    TaskDTO findByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

}
