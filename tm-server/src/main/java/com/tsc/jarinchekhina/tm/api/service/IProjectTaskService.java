package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    TaskDTO bindTaskByProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void clearProjects(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO unbindTaskByProjectId(@Nullable String userId, @Nullable String taskId);

}
