package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    boolean isValid(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session, @Nullable Role role);

    @NotNull
    UserDTO getUser(@Nullable SessionDTO session);

    @NotNull
    String getUserId(@Nullable SessionDTO session);

    @NotNull
    List<SessionDTO> getListSession(@Nullable SessionDTO session);

    void add(@NotNull SessionDTO session);

    @Nullable
    SessionDTO findById(@NotNull String id);

    void removeById(@NotNull String id);

    void clear();

    void close(@Nullable SessionDTO session);

    void closeAll(@NotNull List<SessionDTO> sessionList);

}
