package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<SessionDTO> {

    @Insert("INSERT INTO `tm_session` " +
            "(`id`, `userId`, `signature`, `timestamp`) " +
            "VALUES (#{id}, #{userId}, #{signature}, #{timestamp})")
    void insert(@NotNull SessionDTO session);

    @Update("UPDATE `tm_session` " +
            "SET `userId` = #{userId}, `signature` = #{signature}, `timestamp` = #{timestamp} " +
            "WHERE `id` = #{id}")
    void update(@NotNull SessionDTO session);

    @Delete("DELETE FROM `tm_session`")
    void clear();

    @Delete("DELETE FROM `tm_session` WHERE `id` = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM `tm_session`")
    List<SessionDTO> findAll();

    @NotNull
    @Select("SELECT * FROM `tm_session` WHERE `userId` = #{userId}")
    List<SessionDTO> findByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM `tm_session` WHERE `id` = #{id} LIMIT 1")
    SessionDTO findById(@NotNull @Param("id") String id);

}
