package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService extends IService<UserDTO> {

    boolean checkRoles(@Nullable String userId, @Nullable Role... roles);

    @NotNull
    UserDTO add(@NotNull UserDTO user);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    List<UserDTO> findAll();

    @NotNull
    UserDTO findById(@NotNull String id);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    UserDTO lockByLogin(@Nullable String login);

    void removeById(@NotNull String id);

    void removeByLogin(@Nullable String login);

    @NotNull
    UserDTO setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    UserDTO unlockByLogin(@Nullable String login);

    @NotNull
    UserDTO updateUser(
            @Nullable String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

}
