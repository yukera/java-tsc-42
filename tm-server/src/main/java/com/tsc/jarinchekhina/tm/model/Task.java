package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
public class Task extends AbstractBusinessEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    private Project project;

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date created = new Date();

}
