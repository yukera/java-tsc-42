package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.dto.AbstractEntityDTO;

public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {
}
