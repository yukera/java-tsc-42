package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService extends IService<TaskDTO> {

    void add(@Nullable String userId, @Nullable TaskDTO task);

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable String userId);

    void remove(@Nullable String userId, @Nullable TaskDTO task);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    TaskDTO findById(@Nullable String userId, @Nullable String id);

    @NotNull
    TaskDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateTaskByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    void startTaskById(@Nullable String userId, @Nullable String id);

    void startTaskByIndex(@Nullable String userId, @Nullable Integer index);

    void startTaskByName(@Nullable String userId, @Nullable String name);

    void finishTaskById(@Nullable String userId, @Nullable String id);

    void finishTaskByIndex(@Nullable String userId, @Nullable Integer index);

    void finishTaskByName(@Nullable String userId, @Nullable String name);

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void changeTaskStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
