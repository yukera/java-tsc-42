package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Insert("INSERT INTO `tm_user` " +
            "(`id`, `login`, `passwordHash`, `email`, `firstName`, `lastName`, `middleName`, `role`, `locked`) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, " +
            "#{locked})")
    void insert(@NotNull UserDTO user);

    @Update("UPDATE `tm_user` " +
            "SET `login` = #{login}, `passwordHash` = #{passwordHash}, `email` = #{email}, `firstName` = #{firstName}, " +
            "`lastName` = #{lastName}, `middleName` = #{middleName}, `role` = #{role}, `locked` = #{locked} " +
            "WHERE `id` = #{id}")
    void update(@NotNull UserDTO user);

    @Delete("DELETE FROM `tm_user` WHERE `id` = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM `tm_user` WHERE `login` = #{login}")
    void removeByLogin(@NotNull @Param("login") String login);

    @NotNull
    @Select("SELECT * FROM `tm_user`")
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `id` = #{id} LIMIT 1")
    UserDTO findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `login` = #{login} LIMIT 1")
    UserDTO findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM `tm_user` WHERE `email` = #{email} LIMIT 1")
    UserDTO findByEmail(@NotNull @Param("email") String email);

}
