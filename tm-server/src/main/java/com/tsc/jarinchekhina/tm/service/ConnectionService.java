package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ISessionRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.Task;
import com.tsc.jarinchekhina.tm.model.User;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.soap.SOAPBinding;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private  final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        sqlSessionFactory = getSqlSessionFactory();
        entityManagerFactory = factory();
    }

    @NotNull
    private EntityManagerFactory factory() {
        final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, serviceLocator.getPropertyService().getJdbcDriver());
        settings.put(org.hibernate.cfg.Environment.URL, serviceLocator.getPropertyService().getJdbcUrl());
        settings.put(org.hibernate.cfg.Environment.USER, serviceLocator.getPropertyService().getJdbcUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, serviceLocator.getPropertyService().getJdbcPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, serviceLocator.getPropertyService().getHibernateDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, serviceLocator.getPropertyService().getHibernateHbm2ddl());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, serviceLocator.getPropertyService().getHibernateShow());

        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);

        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);

        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);

        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);

        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String username = serviceLocator.getPropertyService().getJdbcUsername();
        @Nullable final String password = serviceLocator.getPropertyService().getJdbcPassword();
        @Nullable final String url = serviceLocator.getPropertyService().getJdbcUrl();
        @Nullable final String driver = serviceLocator.getPropertyService().getJdbcDriver();
        final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

}
