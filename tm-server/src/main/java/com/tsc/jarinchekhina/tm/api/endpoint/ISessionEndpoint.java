package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionEndpoint extends IEndpoint<SessionDTO> {

    void closeSession(@Nullable SessionDTO session);

    @NotNull
    UserDTO getUser(@Nullable SessionDTO session);

    @Nullable
    SessionDTO openSession(@Nullable String login, @Nullable String password);

}
