package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository extends IRepository<ProjectDTO> {

    @Insert("INSERT INTO `tm_project` " +
            "(`id`, `name`, `description`, `dateStart`, `dateFinish`, `userId`, `status`) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{status})")
    void insert(@NotNull ProjectDTO project);

    @Update("UPDATE `tm_project` " +
            "SET `userId` = #{userId}, `name` = #{name}, `description` = #{description}, `dateStart` = #{dateStart}, " +
            "`dateFinish` = #{dateFinish}, `status` = #{status} " +
            "WHERE `id` = #{id}")
    void update(@NotNull ProjectDTO project);

    @Delete("DELETE FROM `tm_project` WHERE `userId` = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `tm_project` WHERE `userId` = #{userId} AND `id` = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM `tm_project` WHERE `userId` = #{userId} AND `name` = #{name}")
    void removeByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @NotNull
    @Select("SELECT * FROM `tm_project` WHERE `userId` = #{userId}")
    List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `userId` = #{userId} AND `id` = #{id} LIMIT 1")
    ProjectDTO findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    ProjectDTO findByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM `tm_project` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    ProjectDTO findByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

}