package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {
}
